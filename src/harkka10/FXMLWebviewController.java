/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka10;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebHistory.Entry;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Topi
 */
public class FXMLWebviewController implements Initializable {
    @FXML
    private Button searchButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TextField addressInputField;
    @FXML
    private WebView web;
    @FXML
    private Button nextPageButton;
    @FXML
    private Button previousPageButton;
    @FXML
    private Button localFileButton;
    @FXML
    private Button shoutOutButton;
    @FXML
    private Button initializeButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.google.fi");
    }    

    @FXML
    private void searchAction(ActionEvent event) {
        
        if(addressInputField.getText().equals("index2.html") || addressInputField.getText().equals("index.html")) {
        web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        }
        else{
        String userGivenAddress = addressInputField.getText();
        String completeAddress = "http://" + userGivenAddress;
        web.getEngine().load(completeAddress); 
    }
    }

    @FXML
    private void refreshAvtion(ActionEvent event) {
        String currentSite = web.getEngine().getLocation();
        web.getEngine().load(currentSite);
    
    }

    @FXML
    private void nextPageAction(ActionEvent event) {
        int x = web.getEngine().getHistory().getEntries().size() - 1;
        if(web.getEngine().getHistory().getCurrentIndex() < x) {
        web.getEngine().getHistory().go(1);
        }
    }

    @FXML
    private void previousPageAction(ActionEvent event) {
        if(web.getEngine().getHistory().getCurrentIndex() > 0) {
        web.getEngine().getHistory().go(-1);
    }
   }

    @FXML
    private void loadLocalFileAction(ActionEvent event) {
    web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        
    }

    @FXML
    private void shoutOutAction(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void initializeAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }
    
    
    
}
